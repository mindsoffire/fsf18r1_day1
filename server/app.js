console.log("Hello World");

setTimeout(function(){ console.log("3000 ms later"); }, 3000);
// above runs an anonymous callback function declared in args param passed into setTimeout
// setTimeout(function(){ alert("3000 ms later"); }, 3000); it crashed if this is included, y?
// check out where this console.log executes!


var x = {
    name : "JSON Javascript Obj Notn",
    protocol : "figure out how 'secure' in 'https' works",
    y : ["name", "address", false, 2000],
    z : [3, "Heloo", true, 24.55223]
}

var y = {
    name : "Andrew Tan",
    addr : false,
    numerical : ["here", "are", "some", "strings"]
}

console.log(x);
// above prints x in <var x> as reference for comparison

function printx(){
    let x = [y.name, y.numerical[2]];
    // let x = [x.name, y.numerical[2]]; this will crash since x.name at this junct undef
    console.log(x);
}

printx();
// above prints x in <let x> "what's the term here?" scope

function callback(){
    printx();
    console.log(i + "ms later");
}

console.log(x.name);
// above should print back x.name in <var x>
console.log(x[0]); // obj cannot use array notation reference to object's non-array items

let i = 7000;

setTimeout(callback, i);
// setTimeout at 7000ms later to printx x in <let x>
// NOTE: cannot use x in place of i : let x = 7000 since scope would be 'overwritten' and crash timeout
